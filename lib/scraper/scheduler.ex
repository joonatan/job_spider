defmodule Scraper.Scheduler do
  require Logger

  use GenServer

  # delay between checks
  @delay 180
  # Only crawl once an hour
  @update_interval 3600 # seconds

  def start_link(_opts) do
    # TODO should have a parameter for the time interval
    GenServer.start_link(__MODULE__, 0, name: :scheduler)
  end

  @impl true
  def init(state) do
    # need a delay before doing anything big
    schedule_work()

    {:ok, state}
  end

  @impl true
  def handle_info(:work, state) do
    work()
    schedule_work()

    {:noreply, state}
  end

  defp schedule_work() do
    Process.send_after(self(), :work, @delay * 1000)
  end

  defp time_diff?(nil), do: @update_interval + 1
  defp time_diff?(last_updated), do: DateTime.diff(DateTime.utc_now, last_updated)

  defp update(time_diff) when time_diff > @update_interval do
      Logger.info "Spider starting to Crawl."
      Scraper.crawl
      Scraper.parse
      Scraper.Config.updated
      Logger.info "Spider is all DONE."
  end
  defp update(time_diff) do
    Logger.info "Spider is not yet time to crawl: #{@update_interval - time_diff} seconds to go."
  end

  defp work() do
    Scraper.Config.last_updated?
    |> time_diff?
    |> update
  end
end
