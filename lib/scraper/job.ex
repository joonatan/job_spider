defmodule Scraper.Job do
  use Ecto.Schema

  import Logger
  import Ecto.Changeset
  import Ecto.Query
  alias Scraper.{Job, Repo}

  schema "jobs" do
    field :title,  :string
    field :company,  :string
    field :desc,  :string
    field :location,  :string
    field :city,  :string
    field :url,  :string
    field :json_url, :string
    field :source_name, :string
    field :valid_from,  :date
    field :valid_to,  :date
    field :ext_id,  :string
  end

  def parse_date str do
    {:ok, date} = case str do
      nil -> Date.new(0, 1, 1)
      "" -> Date.new(0, 1, 1)
      _ -> Date.from_iso8601(str)
    end
    date
  end

  def changeset(job, params \\ %{}) do
    job
    |> cast(params, [:title, :company, :desc, :location, :city, :url, :json_url, :valid_from, :valid_from, :valid_to, :ext_id])
    |> validate_required([:title, :url])
    |> unique_constraint(:ext_id)
  end

  defp search_by_term(query, []), do: query
  defp search_by_term(query, [x | xs]) do
    query |> search_by_term(x) |> search_by_term(xs)
  end
  defp search_by_term(query, "-" <> term) do
    query
    |> where([j], not ilike(j.desc, ^("%#{term}%")))
    |> where([j], not ilike(j.city, ^("%#{term}%")))
    |> where([j], not ilike(j.company, ^("%#{term}%")))
    |> where([j], not ilike(j.title, ^("%#{term}%")))
  end
  defp search_by_term(query, term) do
    query
    |> where([j], ilike(j.title, ^("%#{term}%"))
      or ilike(j.city, ^("%#{term}%"))
      or ilike(j.company, ^("%#{term}%"))
      or ilike(j.desc, ^("%#{term}%")))
  end

  defp order(query, ""), do: order(query, nil)
  defp order(query, nil) do
    query |> order_by([p], [:valid_to])
  end
  defp order(query, term) do
    query |> order_by([p], [^(String.to_atom(term))])
  end

  defp split_search_exp("\"" <> exp) do
    exp |> String.trim("\"") |> String.trim
  end
  defp split_search_exp(exp) do
    exp |> String.split |> Enum.map(fn x -> x |> String.trim end)
  end

  # finds all patterns in a string separately (AND matching)
  # case-insensitive
  # only tries to find in description
  defp search(query, nil), do: query
  defp search(query, pattern) do
    # get individual search terms with " marking a sentence term
    re = ~r/(\"(?<b>[^"]+)\")/iu
    search_terms = String.split(pattern, re, [include_captures: true, trim: true])
                   |> Enum.map(fn(str) -> split_search_exp(str) end)
                   |> List.flatten
                   |> Enum.filter(fn x -> x != "" end)

    Logger.info("search terms: #{inspect search_terms}")

    query
    |> search_by_term(search_terms)
  end

  defp filter_by(query, _, nil), do: query
  defp filter_by(query, _atom, []), do: query
  defp filter_by(query, atom, list) when is_list(list) do
    [h | t] = list
    case atom do
      :valid_from -> query |> filter_by(atom, h)
      :valid_to -> query |> filter_by(atom, h)
      _ ->
        query = query |> filter_by(atom, h)
        Enum.reduce(t, query, fn(x, acc) ->
          acc |> or_where([j], ilike(field(j, ^atom), ^("%#{x}%")))
        end)
    end
  end
  defp filter_by(query, atom, name) when atom == :valid_to or atom == :valid_from do
    query |> where([j], (field(j, ^atom) >= ^name))
  end
  defp filter_by(query, atom, name) do
    query |> where([j], ilike(field(j, ^atom), ^("%#{name}%")))
  end

  # Supports multiple filter parameters for same value
  #   so city=Helsinki city=Tampere returns everything in both cities
  #   or valid_to=2019-05-19 valid_to=2019-05-01 == valid_to=2019-05-01
  defp filter(query, nil), do: query
  defp filter(query, ""), do: query
  defp filter(query, filter) do
    Logger.info("filter: #{inspect filter}")

    # TODO this doesn't support full sentences (using quotations marks ")
    ls = String.split(filter) |> Enum.filter(fn(x) -> x != "" end) |> Enum.sort
    Logger.info("filter list: #{inspect ls}")

    list = filter_regex(ls) |> Map.to_list
    query |> filter_impl(list)
  end

  defp filter_reg(nil, nil, nil, nil), do: {nil, nil}
  defp filter_reg(city, nil, nil, nil), do: {:city, city}
  defp filter_reg(nil, company, nil, nil), do: {:company, company}
  defp filter_reg(nil, nil, valid_from, nil) do
    {:ok, d} = Date.from_iso8601(valid_from)
    {:valid_from, d}
  end
  defp filter_reg(nil, nil, nil, valid_to) do
    {:ok, d} = Date.from_iso8601(valid_to)
    {:valid_to, d}
  end

  # Convert filters from strings to filter_name: [filters] map
  # valid names are :city, :company, :valid_from, :valid_to
  # or use [{name, filter}] map
  # TODO
  #   atom checking should be in one place and use those atoms for regex
  #   so we have :company, :city, :valid_from, :valid_to atoms
  defp filter_regex(list) when is_list(list), do: filter_regex(list, %{})
  defp filter_regex([], acc), do: acc
  defp filter_regex([x | xs], acc) do
    # TODO combine the Regex so it returns the map of all the filters
    company = (Regex.named_captures(~r/company=(?<company>.+)/iu, x) || %{})["company"]
    city = (Regex.named_captures(~r/city=(?<city>.+)/iu, x) || %{})["city"]
    valid_from = (Regex.named_captures(~r/valid_from=(?<valid_from>.+)/iu, x) || %{})["valid_from"]
    valid_to = (Regex.named_captures(~r/valid_to=(?<valid_to>.+)/iu, x) || %{})["valid_to"]

    elem = filter_reg(city, company, valid_from, valid_to)

    update_map = fn
      acc, {nil, _} -> acc
      acc, {key, val} ->
        if is_nil(acc[key]) do
          Map.put(acc, key, [val])
        else
          update_in acc[key], fn list -> list ++ [val] end
        end
      end

    acc = update_map.(acc, elem)
    filter_regex(xs, acc)
  end

  defp filter_impl(query, nil), do: query
  defp filter_impl(query, []), do: query
  defp filter_impl(query, [{atom, filter} | xs]) do
    query
    |> filter_by(atom, filter)
    |> filter_impl(xs)
  end

  def get(%{:sort => sort, :search => terms, :filter => filter}) do
    Job
    |> filter(filter)
    |> search(terms)
    |> order(sort)
    |> Repo.all
  end
  def get(%{:sort => "", :search => ""}) do
    Job |> Repo.all
  end
  def get(%{:sort => sort, :search => terms}) do
    Job
    |> search(terms)
    |> order(sort)
    |> Repo.all
  end
  def get, do: Job |> Repo.all

end
