defmodule Scraper.Repo do
  use Ecto.Repo,
    otp_app: :scraper,
    adapter: Ecto.Adapters.Postgres
end

# Save application data and configuration so it persists even if run using different commands
# or if the Supervisor needs to restart the process due to an error.
defmodule Scraper.Config do
  use Ecto.Schema
  import Ecto.Query
  import Ecto.Changeset

  schema "config" do
    field :last_updated,  :utc_datetime
  end

  def get_temp, do: "temp/"

  # since config is supposed to only hold single values we use Repo.one exclusively
  def last_updated? do
    Scraper.Config
    |> select([c], c.last_updated)
    |> Scraper.Repo.one
  end

  def changeset(config, params \\ %{}) do
    config
    |> cast(params, [:last_updated])
    |> validate_required([:last_updated])
  end

  def updated do
    case Scraper.Repo.get(Scraper.Config, 1) do
      nil -> %Scraper.Config{id: 1}
      config -> config
    end
    |> Scraper.Config.changeset(%{last_updated: DateTime.utc_now})
    |> Scraper.Repo.insert_or_update!

  end
end
