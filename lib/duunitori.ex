defmodule Duunitori do
  @root "https://duunitori.fi"
  @folder "temp/duunitori"

  # Parsers
  def get_prop(header_info, prop) do
    try do
      {"meta", foo, []} =
        header_info
        |> Floki.find("[itemprop=#{prop}]")
        |> hd
      {"content", valid_from} = foo |> tl |> hd
      valid_from
    rescue
      _ -> ""
    end
  end

  def parse_ad html do
    job_desc = html
      |> Floki.find(".description--jobentry")
      |> Floki.text

    info =
      html
      |> Floki.find(".header__info")

    addr = get_prop(info, "streetAddress")
    valid_till = get_prop(info, "validThrough")
    valid_from = get_prop(info, "datePosted")
    city = get_prop(info, "addressLocality")

    title =
      html
      |> Floki.find(".header__title")
      |> Floki.text

    {"meta", foo, []} =
      html
      |> Floki.find("meta")
      |> Floki.find("[property=\"article:author\"]")
      |> hd
    {"content", comp} = foo |> tl |> hd

    {"meta", foo, []} =
      html
      |> Floki.find("meta")
      |> Floki.find("[property=\"og:url\"]")
      |> hd
    {"content", url} = foo |> tl |> hd

    id = "Duunitori-" <> String.slice(url, -7, 7)

    %{company: comp,
      title: title,
      desc: job_desc,
      city: city,
      location: addr,
      url: url,
      source_name: "Duunitori",
      valid_from: valid_from,
      valid_to: valid_till,
      ext_id: id}
  end

  # HTTP getters
  #
  def get_n_jobs html do
    {"h5", [{"class", "heading__small"}], foo} =
      html
      |> Floki.find(".grid h5")
      |> hd

    {"b", [], [bar]} = foo |> tl |> hd
    bar |> String.to_integer
  end

  # Search towns give by a list param
  # only searches ohjelmointi jobs
  # downloads all of them into temp
  #
  # Save the job adverts to temp
  # under duunitori/{id}
  def get_jobs [x | xs] do
    get_jobs(xs) ++ [get_jobs(x)]
  end
  def get_jobs([]) do [] end
  def get_jobs(town) do
    IO.puts "getting jobs for #{town}"

    if not File.exists?(@folder) do
      File.mkdir!(@folder)
    end

    opts = [recv_timeout: 30000]
    page = 1
    search_url = @root <> "/tyopaikat/?haku=ohjelmointi+ja+ohjelmistokehitys+%28ala%29&alue=#{town}&sivu="#{page}"
    first_page_uri = search_url <> Integer.to_string(page)
    html = first_page_uri |> HTTPoison.get!([], opts)

    pages = get_n_jobs(html.body) / 20 |> Float.ceil |> Kernel.trunc

    uris = Enum.map((1..pages), fn(x) -> search_url <> Integer.to_string(x) end)
    uris
    |> Enum.map(fn(uri) -> HTTPoison.get!(uri, [], opts) end)
    |> Enum.map(fn(html) -> get_individual_jobs(html.body, @root, @folder) end)
  end


  def get_individual_jobs(html, root_uri, folder) do
    # Save the individual job pages to temp
    # urls last seven characters are an id
    # take the body and save it to temp/duunitori/id
    # Filter out any pages we already have stored to make this go a bit faster
    html
    |> Floki.find("a.job-box__hover")
    |> Floki.attribute("href")
    |> Enum.map(fn(url) -> {String.slice(url, -7, 7), url} end)
    |> Enum.filter(fn({name, _url}) -> not File.exists?(folder <> "/" <> name) end)
    |> Enum.map(fn({name, url}) -> {name, HTTPoison.get!(root_uri <> url)} end)
    |> Enum.map(fn({name, html}) -> File.write!(folder <> "/" <> name, html.body) end)
  end
end
