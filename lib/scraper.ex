defmodule Scraper do
  require Logger

  @moduledoc """
  Documentation for Scraper.
  """

  @doc """
    Crawl web sites: currently only Duunitori and pull their ads
  """
  def crawl do
    Logger.info "Crawl"

    {:ok, time} = timeit(&crawl_impl/0)
    Logger.info "Crawling took: #{time} seconds"
    :ok
  end

  defp crawl_impl do
    fold_name = "temp"
    if File.exists?(fold_name) and not File.dir?(fold_name) do
      IO.puts "Can't download: #{fold_name} not a folder"
      :error
    else
      if not File.exists?(fold_name) do
        File.mkdir!(fold_name)
      end
        Duunitori.get_jobs ["tampere", "helsinki"]
        Te.scrape
        :ok
    end
  end

  def timeit(fun) do
    {time, val} = :timer.tc(fun)
    {val, time |> Kernel./(1_000_000)}
  end

  # TODO this could be update instead of insert too
  # Can't use insert_or_update since we have custom primary-keys
  #   need to change our keys to the ext_id
  defp insert_and_ignore(cs) do
    case Scraper.Repo.insert(cs) do
      {:ok, change} -> change
      {:error, change} ->
        #IO.puts "Exception raised when inserting #{cs} changeset to Ecto.Repo"
        change
    end
  end

  # Parse the temp html files in "/temp" to SQL entities
  # and push them to Ecto Repo
  def parse do
    Logger.info "Parse"

    {val, time} = timeit(&parse_impl/0)
    Logger.info "Parsing took: #{time} seconds"

    val
  end

  defp parse_impl do
    # Duunitori
    folder = Scraper.Config.get_temp <> "duunitori"

    File.ls!(folder)
    |> Enum.filter(fn(file) -> String.slice(file, -4, 4) != ".swp" end)
    |> Enum.map(fn(file) -> folder <> "/" <> file end)
    |> Enum.map(fn(path) -> File.read!(path) end)
    |> Enum.map(fn(html) -> Duunitori.parse_ad(html) end)
    |> Enum.map(fn(x) -> insert_and_ignore(Scraper.Job.changeset(%Scraper.Job{}, x)) end)

    # TE-keskus
    Te.parse
    |> Enum.map(fn(x) -> insert_and_ignore(Scraper.Job.changeset(%Scraper.Job{}, x)) end)
  end

end
