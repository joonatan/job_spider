defmodule Te do
  # JSON url (taken from their website)
  @url "https://paikat.te-palvelut.fi/tpt-api/tyopaikat?valitutAmmattialat=25&valitutAmmattialat=35&ilmoitettuPvm=1&vuokrapaikka=--"

  def scrape do
    IO.puts "Scraping: TE-keskus"

    json = HTTPoison.get!(@url)
    # dump it for later use
    out_fold = Scraper.Config.get_temp
    if not File.exists?(out_fold), do: File.mkdir(out_fold)

    File.write!(out_fold <> "te.json", json.body, [:binary])
  end

  defp parse_ad x do
    id = x["ilmoitusnumero"]

    job_desc = x["kuvausteksti"]

    # date format: "29.05.2019 klo 16:00" : need 2019-05-29 (forget about the clock)
    valid_to = if is_nil(x["hakuPaattyy"]) do
      nil
    else
      [d, m, y] = String.split(hd(String.split(x["hakuPaattyy"], " klo")), ".")
      valid_to = "#{y}-#{m}-#{d}"
    end

    %{company: x["tyonantajanNimi"],
      title: x["tehtavanimi"],
      desc: job_desc,
      city: x["kunta"],
      location: x["kunta"],
      valid_from: x["ilmoituspaivamaara"],
      valid_to: valid_to,
      url: "https://paikat.te-palvelut.fi/tpt/#{id}",
      source_name: "TE",
      json_url: "https://paikat.te-palvelut.fi/tpt-api/tyopaikat/#{id}?kieli=fi",
      ext_id: "te-#{id}"
      }
  end

  def parse do
    json = File.read!(Scraper.Config.get_temp <> "te.json")
    body = Poison.Parser.parse!(json, %{})

    body["response"]["docs"]
    |> Enum.map(fn x -> parse_ad x end)
  end
end
