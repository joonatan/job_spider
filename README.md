# Scraper

Testing web crawling and html parsing.

Parsing job ads from different sites: especially Duunitori

Why? Because their site is slow as fuck.

Other reasons: too much javascript, horrible search options etc.

## Use

Populates a database 'crawler_dev' using the default user (postgres) defined in config/config.exs

Polls periodically and executes the Spider once an hour.

```
mix deps.get
mix compile
iex -S mix run
```

Be sure you have the database  and tables created first
```
mix deps.get
mix compile
mix ecto.create
mix ecto.migrate
```

If your psql user is something else than default change it in: config/config.exs

Same if you are using a remote PSQL database.

## How
* Uses HTTPoison for getting web pages
* Floki for parsing those pages
* Ecto and SQL to save them to a persistent format

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `scraper` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:scraper, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/scraper](https://hexdocs.pm/scraper).

