ExUnit.start()

Ecto.Adapters.SQL.Sandbox.mode(Scraper.Repo, :manual)

defmodule Test.Helpers do
  def clean_repo do
    Scraper.Job |> Scraper.Repo.delete_all
    Scraper.Config |> Scraper.Repo.delete_all
    :ok
  end

  def kill_scheduler do
    :ok = Supervisor.stop(:scheduler)
  end
end
