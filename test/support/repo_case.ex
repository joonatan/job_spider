defmodule Scraper.RepoCase do
  use ExUnit.CaseTemplate
  
  using do
    quote do
      alias Scraper.Repo

      import Ecto
      import Ecto.Query
      import Scraper.RepoCase
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Scraper.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Scraper.Repo, {:shared, self()})
    end

    :ok
  end
end
