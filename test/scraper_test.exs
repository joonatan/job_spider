defmodule ScraperTest do
  use ExUnit.Case
  doctest Scraper

  setup_all do
    Test.Helpers.kill_scheduler
  end

  test "timeit runs a function" do
    {val, time} = Scraper.timeit (fn -> 2 +2 end)
    assert val == 4
    assert time >= 0
    assert time < 1
  end

  test "timeit measures properly" do
    {val, time} = Scraper.timeit (fn -> Process.sleep(1000); 2 + 2 end)
    assert val == 4
    assert time > 1
    assert time < 2
  end
end
