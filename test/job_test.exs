defmodule ScraperJobTest do
  use ExUnit.Case, async: false

  use Scraper.RepoCase
  doctest Scraper
  alias Scraper.{Job, Repo}

  setup_all do
    Test.Helpers.kill_scheduler
  end

  setup do
    Test.Helpers.clean_repo
  end

  defp data(name, id, city \\ "LA", company \\ "Big ass company", opts \\ %{}) do
    %{company: company,
      title: name,
      desc: "Not a description",
      city: city,
      location: "On a sunny beach",
      valid_from: opts[:valid_from] || "",
      valid_to: opts[:valid_to] || "",
      url: "https://not_a_valid_address.nowhere",
      source_name: "EXTRATERESTIAL",
      json_url: "https://not_a_valid_address.nowhere:json",
      ext_id: "EXT-#{id}"
      }
  end

  defp add_data(name, id, city \\ "LA", company \\ "Big ass company", opts \\ %{}) do
    %Job{} |> Job.changeset(data(name, id, city, company, opts)) |> Repo.insert!
  end

  # Job tests
  test "parse date handles nil" do
    assert Job.parse_date(nil)== ~D[0000-01-01]
  end

  test "parse date handles iso" do
    assert Job.parse_date("2019-05-30") == ~D[2019-05-30]
  end

  test "get with nil params is all" do
    add_data("Great Job", 1)
    add_data("Not a Great Job", 2)
    add_data("Just another day", 3)

    jobs = Job.get(%{sort: nil, search: nil})
    assert length(jobs) == 3
    assert hd(jobs).title == "Great Job"
    assert hd(tl(jobs)).title == "Not a Great Job"
    assert hd(tl(tl(jobs))).title == "Just another day"

    j1 = hd(jobs)
    j2 = data("Great Job", 1)
    assert j1.title    == j2.title
    assert j1.company  == j2.company
    assert j1.desc     == j2.desc
    assert j1.city     == j2.city
    assert j1.location == j2.location
    assert j1.ext_id   == j2.ext_id
    assert j1.url      == j2.url
    assert j1.json_url == j2.json_url
    assert j1.valid_to == nil
    assert j1.valid_from == nil
  end

  test "get without parameters is get all" do
    add_data("Great Job", 1)
    add_data("Not a Great Job", 2)
    add_data("Just another day", 3)

    assert length(Job.get()) == 3
  end

  test "get with empty string params" do
    add_data("Great Job", 1)
    add_data("Not a Great Job", 2)
    add_data("Just another day", 3)

    jobs = Job.get(%{sort: "", search: ""})
    assert length(jobs) == 3
  end

  test "get with sort" do
    add_data("Not a Great Job", 1)
    add_data("Great Job", 2)
    add_data("Just another day", 3)

    jobs = Job.get(%{sort: "title", search: ""})
    assert hd(jobs).title == "Great Job"
  end

  test "get with search" do
    # need to add test data to database
    add_data("Great Job", 1)
    add_data("Not a Great Job", 2)
    add_data("Just another day", 3)

    jobs = Job.get(%{sort: nil, search: "Great"})
    assert length(jobs) == 2

    jobs = Job.get(%{sort: nil, search: "Just"})
    assert length(jobs) == 1

    # Test the quotation search
    jobs = Job.get(%{sort: nil, search: "\"Not a Great\""})
    assert length(jobs) == 1

    # list of search options
    jobs = Job.get(%{sort: nil, search: "Not a Great"})
    assert length(jobs) == 2

    # negation
    jobs = Job.get(%{sort: nil, search: "-Great"})
    assert length(jobs) == 1

    # case insensitive
    jobs = Job.get(%{sort: nil, search: "great"})
    assert length(jobs) == 2
  end

  test "get with empties" do
    add_data("Great Job", 1)
    add_data("Not a Great Job", 2)
    add_data("Just another day", 3)

    jobs = Job.get(%{sort: nil, search: nil, filter: nil})
    assert length(jobs) == 3

    jobs = Job.get(%{sort: nil, search: nil, filter: ""})
    assert length(jobs) == 3

    jobs = Job.get(%{sort: nil, search: "", filter: ""})
    assert length(jobs) == 3

    jobs = Job.get(%{sort: "", search: nil, filter: ""})
    assert length(jobs) == 3

    jobs = Job.get(%{sort: "", search: nil, filter: nil})
    assert length(jobs) == 3


    jobs = Job.get(%{sort: "", search: "", filter: ""})
    assert length(jobs) == 3
  end

  test "get with city filter" do
    add_data("Great Job", 1, "San Fran")
    add_data("Not a Great Job", 2, "NY")
    add_data("Just another day", 3)
    add_data("Just another day", 4, "NY")

    jobs = Job.get(%{sort: nil, search: nil, filter: "city=LA"})
    assert length(jobs) == 1

    jobs = Job.get(%{sort: nil, search: nil, filter: "city=San Fran"})
    assert length(jobs) == 1

    jobs = Job.get(%{sort: nil, search: nil, filter: "city=NY"})
    assert length(jobs) == 2
  end

  test "get with company filter" do
    add_data("Great Job", 1, "San Fran", "small")
    add_data("Not a Great Job", 2, "NY", "dervish company")
    add_data("Just another day", 3)

    jobs = Job.get(%{sort: nil, search: nil, filter: nil})
    assert length(jobs) == 3

    assert Job.get(%{sort: nil, search: nil, filter: "company="}) |> length == 3

    assert Job.get(%{sort: nil, search: nil, filter: "company=small"}) |> length == 1

    assert Job.get(%{sort: nil, search: nil, filter: "company=dervish"}) |> length == 1

    assert Job.get(%{sort: nil, search: nil, filter: "company=DERVish"}) |> length == 1

    assert Job.get(%{sort: nil, search: nil, filter: "company=dervish company"}) |> length == 1
  end

  test "get with valid_from filter" do
    add_to_repo = fn (id, date) ->
      %Job{}
      |> Job.changeset(%{valid_from: date, title: "not a blank", url: "not a blank", ext_id: "#{id}", city: "LA", company: "Big think"})
      |> Repo.insert!
    end
    add_to_repo.(1, Date.utc_today)
    add_to_repo.(2, Date.add(Date.utc_today, -2))
    add_to_repo.(3, Date.add(Date.utc_today, -3))
    add_to_repo.(4, Date.add(Date.utc_today, -3))

    #assert Job.get(%{sort: nil, search: nil, filter: "valid_from=2019-05-12"}) |> length == 1

    assert Job.get(%{sort: nil, search: nil, filter: "valid_from=#{Date.utc_today}"}) |> length == 1

    assert Job.get(%{sort: nil, search: nil, filter: "valid_from=#{Date.add(Date.utc_today, -2)}"}) |> length == 2

    assert Job.get(%{sort: nil, search: nil, filter: "valid_from=#{Date.add(Date.utc_today, -3)}"}) |> length == 4
  end

  # valid_to is valid_on date, might need a rename on some of these
  # but in the SQL it makes sense to be valid_to, in filter it means things that are still valid
  # on that date
  test "get with valid_to filter" do
    add_to_repo = fn (id, date) ->
      %Job{}
      |> Job.changeset(%{valid_to: date, title: "not a blank", url: "not a blank", ext_id: "#{id}", city: "LA", company: "Big think"})
      |> Repo.insert!
    end
    add_to_repo.(1, Date.utc_today)
    add_to_repo.(2, Date.add(Date.utc_today, -2))
    add_to_repo.(3, Date.add(Date.utc_today, 3))
    add_to_repo.(4, Date.add(Date.utc_today, 3))

    today = Date.utc_today
    tomorrow = Date.add(Date.utc_today, 1)
    three_days = Date.add(Date.utc_today, 3)
    month = Date.add(Date.utc_today, 30)
    assert Job.get(%{sort: nil, search: nil, filter: "valid_to=#{today}"}) |> length == 3

    assert Job.get(%{sort: nil, search: nil, filter: "valid_to=#{tomorrow}"}) |> length == 2

    assert Job.get(%{sort: nil, search: nil, filter: "valid_to=#{three_days}"}) |> length == 2

    assert Job.get(%{sort: nil, search: nil, filter: "valid_to=#{month}"}) |> length == 0
  end

  test "get with combined filter" do
    add_data("Great Job", 1, "San Fran", "small")
    add_data("Not a Great Job", 2, "NY", "dervish company")
    add_data("Just another day", 3, "NY")
    add_data("Just another day", 4, "LA", "dervish")

    assert Job.get(%{sort: nil, search: nil, filter: "city=NY"}) |> length == 2
    assert Job.get(%{sort: nil, search: nil, filter: "company=dervish"}) |> length == 2
    assert Job.get(%{sort: nil, search: nil, filter: "city=NY company=dervish"}) |> length == 1

    # TODO test valid_to and valid_from in combined filters
  end

  test "get with multiple city filters" do
    add_data("Great Job", 1, "San Fran", "small")
    add_data("Not a Great Job", 2, "NY", "dervish company")
    add_data("Just another day", 3, "NY")
    add_data("Just another day", 4, "LA", "dervish")

    assert Job.get(%{sort: nil, search: nil, filter: nil}) |> length == 4
    assert Job.get(%{sort: nil, search: nil, filter: "city=NY city=LA"}) |> length == 3
    assert Job.get(%{sort: nil, search: nil, filter: "city=San city=LA"}) |> length == 2

    # TODO how to handle multi word expressions like: San Fran (they don't work with String.split)
  end

  test "get with multiple company filters" do
    add_data("Great Job", 1, "San Fran", "small")
    add_data("Not a Great Job", 2, "NY", "dervish company")
    add_data("Just another day", 3, "NY", "big")
    add_data("Just another day", 4, "LA", "dervish")

    assert Job.get(%{sort: nil, search: nil, filter: nil}) |> length == 4
    assert Job.get(%{sort: nil, search: nil, filter: "company=dervish company=small"}) |> length == 3
    assert Job.get(%{sort: nil, search: nil, filter: "company=big company=small"}) |> length == 2
  end

  test "get with multiple date filters" do
    add_to_repo = fn (id, date) ->
      %Job{}
      |> Job.changeset(%{valid_to: date, title: "not a blank", url: "not a blank", ext_id: "#{id}", city: "LA", company: "Big think"})
      |> Repo.insert!
    end
    add_to_repo.(1, Date.utc_today)
    add_to_repo.(2, Date.add(Date.utc_today, -2))
    add_to_repo.(3, Date.add(Date.utc_today, 2))
    add_to_repo.(4, Date.add(Date.utc_today, 3))

    # Check that we use inclusive search
    three_days = Date.add(Date.utc_today, 3)
    two_days = Date.add(Date.utc_today, 2)
    one_day = Date.add(Date.utc_today, 1)
    assert Job.get(%{sort: nil, search: nil, filter: "valid_to=#{Date.utc_today}"}) |> length == 3
    assert Job.get(%{sort: nil, search: nil, filter: "valid_to=#{three_days}"}) |> length == 1
    assert Job.get(%{sort: nil, search: nil, filter: "valid_to=#{two_days}"}) |> length == 2

    assert Job.get(%{sort: nil, search: nil, filter:
      "valid_to=#{Date.utc_today} valid_to=#{three_days}"}) |> length == 3

    assert Job.get(%{sort: nil, search: nil, filter:
      "valid_to=#{one_day} valid_to=#{two_days}"})
      |> length == 2
  end

  test "get with multiple mixed filters" do
    add_data("Great Job", 1, "San Fran", "small", valid_to: "2019-05-11")
    add_data("Not a Great Job", 2, "NY", "dervish company", valid_to: "2019-05-10")
    add_data("Just another day", 3, "NY", "big", valid_to: "2019-05-12")
    add_data("Just another day", 4, "LA", "dervish", valid_to: "2019-05-13")
    add_data("Just another", 5, "LA", "dervish", valid_to: "2019-05-13")

    assert Job.get(%{sort: nil, search: nil, filter: "valid_to=2019-05-10"}) |> length == 5
    assert Job.get(%{sort: nil, search: nil, filter: "valid_to=2019-05-12"}) |> length == 3
    assert Job.get(%{sort: nil, search: nil, filter: "valid_to=2019-05-13"}) |> length == 2

    assert Job.get(%{sort: nil, search: nil, filter: "valid_to=2019-05-12 city=LA"}) |> length == 2
    assert Job.get(%{sort: nil, search: nil, filter: "valid_to=2019-05-10 city=NY"}) |> length == 2

    assert Job.get(%{sort: nil, search: nil, filter: "city=NY city=LA"}) |> length == 4
    assert Job.get(%{sort: nil, search: nil, filter: "valid_to=2019-05-11 city=NY city=LA"}) |> length == 3
    assert Job.get(%{sort: nil, search: nil, filter: "city=NY city=LA valid_to=2019-05-11   "}) |> length == 3

    assert Job.get(%{sort: nil, search: nil, filter: "garbage valid_to=2019-05-10 city=NY"}) |> length == 2
  end
  # TODO the filter fails because it creates a query like
  # where (city=&1 and valid_to=&0) or city=&2
  # we need: where (city=&1 or city=&2) and valid_to=&0

  # TODO add predefined variables like :today, :yesterday, :this_month
  # available for both valid_from and valid_to
end
