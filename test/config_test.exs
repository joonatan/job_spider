defmodule Scraper.ConfigTest do
  use ExUnit.Case, async: false

  use Scraper.RepoCase
  doctest Scraper.Config

  alias Scraper.{Config, Repo}

  setup_all do
    Test.Helpers.kill_scheduler
  end

  setup do
    Test.Helpers.clean_repo
  end

  test "last_updated returns nil" do
    assert Config.last_updated? == nil
  end

  test "last_updated is now" do
    Config.updated
    secs = DateTime.diff(Config.last_updated?, DateTime.utc_now)
    assert secs < 1
  end

  test "config is unique" do
    Config.updated
    Config.updated
    Config.updated
    assert Config |> Repo.all |> length == 1
  end
end

