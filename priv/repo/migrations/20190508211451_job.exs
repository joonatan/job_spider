defmodule Scraper.Repo.Migrations.Job do
  use Ecto.Migration

  def change do
    create table(:jobs) do
      add :title,  :string
      add :company,  :string
      add :desc,  :text
      add :location,  :string
      add :city, :string
      add :url,  :string
      add :valid_from,  :date
      add :valid_to,  :date
      add :json_url, :string
      add :source_name, :string
      add :ext_id,  :string

    end

    create unique_index(:jobs, [:ext_id])

  end
end
