defmodule Scraper.Repo.Migrations.Config do
  use Ecto.Migration

  def change do
    create table(:config) do
      add :last_updated, :utc_datetime
    end
  end
end
