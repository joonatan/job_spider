use Mix.Config

#config :scraper, :env, :test

config :logger, level: :warn

config :scraper, Scraper.Repo,
  database: "scraper_test",
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
